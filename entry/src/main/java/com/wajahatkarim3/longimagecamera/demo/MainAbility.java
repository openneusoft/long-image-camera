package com.wajahatkarim3.longimagecamera.demo;

import com.wajahatkarim3.longimagecamera.demo.slice.MainAbilitySlice;
import com.wajahatkarim3.longimagecamera.slice.LongImageAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    @Override
    public final void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());

        addActionRoute("action.longimage", LongImageAbilitySlice.class.getName());
    }

}

package com.wajahatkarim3.longimagecamera.demo.slice;

import com.wajahatkarim3.longimagecamera.demo.ResourceTable;
import com.wajahatkarim3.longimagecamera.cameraview.TouchImageView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.app.AbilityContext;
import ohos.bundle.IBundleManager;
import ohos.data.resultset.ResultSet;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;

public class MainAbilitySlice extends AbilitySlice {
    private static final double SCREEN_WIDTH_DOUBLE = 512.0;
    private static final int SCREEN_WIDTH_INT = 512;

    @Override
    public final void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Button button = (Button) findComponentById(ResourceTable.Id_button_start);
        // 权限申请
        permissionApply(this, 1,
                // 摄像机权限
                "ohos.permission.CAMERA",
                "ohos.permission.WRITE_MEDIA");
        // 调用Camera模块业务层


        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent secondIntent = new Intent();
                // 指定待启动FA的bundleName和abilityName
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName("com.wajahatkarim3.longimagecamera.demo")
                        .withAbilityName("com.wajahatkarim3.longimagecamera.LongImageAbility")
                        .build();
                secondIntent.setOperation(operation);
                // 通过AbilitySlice的startAbility接口实现启动另一个页面
                startAbilityForResult(secondIntent, 0);
            }
        });
    }

    @Override
    public final void onActive() {
        super.onActive();
    }

    @Override
    public final void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected final void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        TouchImageView imageView = (TouchImageView) findComponentById(ResourceTable.Id_imageView);
        switch (requestCode) {
            case 0:
                DataAbilityHelper helper = DataAbilityHelper.creator(this);
                try {
                    ResultSet resultSet = helper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, new String[]{AVStorage.Images.Media.ID}, null);
                    while (resultSet != null && resultSet.goToNextRow()) {
                        int id = resultSet.getInt(resultSet.getColumnIndexForName(AVStorage.Images.Media.ID));
                        Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(id));
                        FileDescriptor fd = helper.openFile(uri, "r");

                        ImageSource.SourceOptions opts = new ImageSource.SourceOptions();
                        ImageSource imageSource = ImageSource.create(fd, opts);
                        ImageSource.DecodingOptions options = new ImageSource.DecodingOptions();
                        options.editable = true;
                        options.sampleSize = 2;

                        PixelMap d = imageSource.createPixelmap(options);
                        int newHeight = (int) ( d.getImageInfo().size.height * (SCREEN_WIDTH_DOUBLE / d.getImageInfo().size.width) );
                        Rect rect = new Rect(0,0,0,0);
                        PixelMap.InitializationOptions init= new PixelMap.InitializationOptions();
                        init.size = new Size(SCREEN_WIDTH_INT, newHeight);
                        //init.size 512;
                        PixelMap putImage = PixelMap.create(d, rect, init);
                        imageView.setImageBitmap(putImage);
                    }

                } catch (DataAbilityRemoteException | FileNotFoundException e) {
                    e.printStackTrace();
                }

            default:
        }
    }

    public static void permissionApply(AbilityContext context, Integer requestCode, String ... permissions){
        // 存储未授权的权限信息
        String[] unauthorizedInfo = new String[permissions.length];
        for(Integer i = 0; i<permissions.length; i++){
            if(context.verifySelfPermission(permissions[i]) != IBundleManager.PERMISSION_GRANTED){
                if(context.canRequestPermission(permissions[i])){
                    unauthorizedInfo[i] = permissions[i];
                }
            }
        }
        // 非空进行授权
        if(unauthorizedInfo != null && unauthorizedInfo.length != 0){
            context.requestPermissionsFromUser(unauthorizedInfo, requestCode);
        }
    }
}

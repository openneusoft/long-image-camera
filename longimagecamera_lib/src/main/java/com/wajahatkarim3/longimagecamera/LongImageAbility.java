package com.wajahatkarim3.longimagecamera;

import com.wajahatkarim3.longimagecamera.slice.LongImageAbilitySlice;
import com.wajahatkarim3.longimagecamera.slice.PreviewLongImageAbiltySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class LongImageAbility extends Ability {
    @Override
    public final void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(LongImageAbilitySlice.class.getName());
        addActionRoute("action.previewlongimage", PreviewLongImageAbiltySlice.class.getName());
    }

    @Override
    protected final void onActive() {
        Intent resultIntent = new Intent();
        setResult(0, resultIntent);   //0为当前Ability销毁后返回的resultCode。
    }
}

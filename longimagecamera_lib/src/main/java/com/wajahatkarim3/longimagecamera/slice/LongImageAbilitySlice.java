package com.wajahatkarim3.longimagecamera.slice;

import com.wajahatkarim3.longimagecamera.ResourceTable;
import com.wajahatkarim3.longimagecamera.cameraview.CameraView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.ProgressBar;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.bundle.AbilityInfo;
import ohos.bundle.IBundleManager;
import ohos.data.rdb.ValuesBucket;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImagePacker;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.*;
import java.util.ArrayList;

public class LongImageAbilitySlice extends AbilitySlice {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 00201, LongImageAbilitySlice.class.getName());

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1002;
    private static final int MY_PERMISSIONS_REQUEST_STORAGE = 1247;
    public static final String IMAGE_PATH_KEY = "imagePathKey";
    public static final int LONG_IMAGE_RESULT_CODE = 1234;

    private static final int DEGREE_RIGHT_ANGLE = 90;

    private static final int MOVE_DELAY_TIME = 100;
    private static final double MOVE_RATIO_FIRST = 0.15;
    private static final double MOVE_RATIO_SECOND = 0.80;
    private static final int MOVE_DURATION = 1500;

    private static final int OPT_QUALITY = 90;

    private static CameraView mCameraView;

    private final String tag = LongImageAbilitySlice.class.getSimpleName();

    public enum ImageMergeMode {
        HORIZONTAL,
        VERTICAL
    }

    private Button btnSnap;
    private Image btnDone, btnFlashMode;
    private Image imgRecent;

    private ProgressBar progressBar;

    private boolean isFirstImage = true;
    private ArrayList<PixelMap> bitmapsList = new ArrayList<>();
    private EventHandler mBackgroundHandler;

    private PixelMap mFinalBitmap;

    private ImageMergeMode mergeMode = ImageMergeMode.VERTICAL;
    private boolean showPreview = false;

    @Override
    public final void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_long_image);
        HiLog.info(LABEL,"111");

        btnSnap = (Button) findComponentById(ResourceTable.Id_btnSnap);
        btnDone = (Image) findComponentById(ResourceTable.Id_btnDone);
        btnFlashMode = (Image) findComponentById(ResourceTable.Id_btnFlashMode);
        imgRecent = (Image) findComponentById(ResourceTable.Id_imgRecent);
        progressBar = (ProgressBar) findComponentById(ResourceTable.Id_progressBar);
        imgRecent.setVisibility(Component.HIDE);
        progressBar.setVisibility(Component.HIDE);
        btnDone.setVisibility(Component.HIDE);

        mCameraView = (CameraView) findComponentById(ResourceTable.Id_cameraview);

        btnSnap.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                btnSnapClick(component);
            }
        });

        btnDone.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    btnDoneClick(component);
                } catch (NotExistException e) {
                    HiLog.error(LABEL, "NotExistException");
                } catch (WrongTypeException e) {
                    HiLog.error(LABEL, "WrongTypeException");
                } catch (IOException e) {
                    HiLog.error(LABEL, "IOException");
                }
            }
        });

        btnFlashMode.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                changeFlashMode();
            }
        });

        checkForCameraPermission();

        mCameraView.addCallback(cameraCallback);
        mCameraView.setFlash(CameraView.FLASH_AUTO);
    }

    @Override
    public final void onActive() {
        super.onActive();
    }

    @Override
    public final void onForeground(Intent intent) {
        super.onForeground(intent);
        checkForCameraPermission();
    }

    @Override
    public final void onInactive() {
        mCameraView.stop();
        super.onInactive();
    }

    @Override
    public final void onStop() {
        mCameraView.stop();
        super.onStop();
    }

    @Override
    public final void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation)
    {
        super.onOrientationChanged(displayOrientation);

        mCameraView.setDisplayOrientation(displayOrientation);
        if(displayOrientation == AbilityInfo.DisplayOrientation.LANDSCAPE) {
            imgRecent.setRotation(DEGREE_RIGHT_ANGLE);
        }
    }

    public final void changeFlashMode()
    {
        if (mCameraView.getFlash() == CameraView.FLASH_AUTO)
        {
            mCameraView.setFlash(CameraView.FLASH_ON);
            btnFlashMode.setPixelMap(ResourceTable.Media_ic_flash_on);
        }
        else if (mCameraView.getFlash() == CameraView.FLASH_ON)
        {
            mCameraView.setFlash(CameraView.FLASH_OFF);
            btnFlashMode.setPixelMap(ResourceTable.Media_ic_flash_off);
        }
        else if (mCameraView.getFlash() == CameraView.FLASH_OFF)
        {
            mCameraView.setFlash(CameraView.FLASH_AUTO);
            btnFlashMode.setPixelMap(ResourceTable.Media_ic_flash_auto);
        }
    }

    public final void btnSnapClick(Component view)
    {
        mCameraView.takePicture();
    }

    public final void btnDoneClick(Component view) throws NotExistException, WrongTypeException, IOException {

        if (bitmapsList.size() <= 0) return;

        if (mergeMode == ImageMergeMode.VERTICAL) {
            int width = bitmapsList.get(0).getImageInfo().size.width;
            int height = 0;

            for (PixelMap bitmap : bitmapsList) {
                height += bitmap.getImageInfo().size.height;
            }
            PixelMap.InitializationOptions init = new PixelMap.InitializationOptions();
            init.size = new Size(width,height);
            init.pixelFormat = PixelFormat.ARGB_8888;
            mFinalBitmap = PixelMap.create(init);
            Texture texture = new Texture(mFinalBitmap);
            Canvas canvas = new Canvas(texture);
            float tempHeight = 0;

            for (int i = 0; i < bitmapsList.size(); i++) {
                PixelMap bitmap = bitmapsList.get(i);
                PixelMapHolder holder = new PixelMapHolder(bitmap);
                Paint paint = new Paint();
                canvas.drawPixelMapHolder(holder, 0f, tempHeight, paint);
                tempHeight += bitmap.getImageInfo().size.height;
            }
        } else if (mergeMode == ImageMergeMode.HORIZONTAL) {
            int height = bitmapsList.get(0).getImageInfo().size.height;
            int width = 0;

            for (PixelMap bitmap : bitmapsList) {
                width += bitmap.getImageInfo().size.width;
            }
            PixelMap.InitializationOptions init = new PixelMap.InitializationOptions();
            init.size = new Size(width,height);
            init.pixelFormat = PixelFormat.ARGB_8888;
            mFinalBitmap = PixelMap.create(init);
            Texture texture = new Texture(mFinalBitmap);
            Canvas canvas = new Canvas(texture);

            float tempWidth = 0;

            for (int i = 0; i < bitmapsList.size(); i++) {
                PixelMap bitmap = bitmapsList.get(i);
                PixelMapHolder holder = new PixelMapHolder(bitmap);
                Paint paint = new Paint();
                canvas.drawPixelMapHolder(holder, 0f, tempWidth, paint);
                tempWidth += bitmap.getImageInfo().size.width;
            }
        }
        checkForFileWritePermission();
    }

    public final void checkForFileWritePermission() throws NotExistException, WrongTypeException, IOException {
        if (verifySelfPermission("ohos.permission.WRITE_MEDIA") == IBundleManager.PERMISSION_GRANTED)
        {
            String[] permissions = {"ohos.permission.READ_USER_STORAGE", "ohos.permission.WRITE_USER_STORAGE"};
            requestPermissionsFromUser(permissions, 0);
            try{
                saveBitmap(mFinalBitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            if (canRequestPermission("ohos.permission.WRITE_MEDIA")) {
                requestPermissionsFromUser(
                        new String[] { "ohos.permission.WRITE_MEDIA" } , MY_PERMISSIONS_REQUEST_STORAGE);
            }
        }
    }

    public final void checkForCameraPermission()
    {
        if (verifySelfPermission("ohos.permission.CAMERA") == IBundleManager.PERMISSION_GRANTED)
        {
            mCameraView.start();
        }
        else if (canRequestPermission("ohos.permission.CAMERA")) {
            showInfoDialog();
        } else {
            requestPermissionsFromUser(
                    new String[] { "ohos.permission.CAMERA" } , MY_PERMISSIONS_REQUEST_CAMERA);
            mCameraView.start();
        }
    }

    public final void showInfoDialog()
    {
        HiLog.info(LABEL, "CAMERA permission");
    }

    private CameraView.Callback cameraCallback = new LongCallback();

    public final void saveBitmap(PixelMap finalBitmap) throws DataAbilityRemoteException, FileNotFoundException {
        btnDone.setEnabled(false);
        btnSnap.setEnabled(false);
        btnFlashMode.setEnabled(false);

        mCameraView.setVisibility(Component.INVISIBLE);
        btnDone.setVisibility(Component.INVISIBLE);
        btnFlashMode.setVisibility(Component.INVISIBLE);
        progressBar.setVisibility(Component.VISIBLE);

        final String tmpImg = String.valueOf(System.currentTimeMillis());

        ResourceManager resourceManager = this.getResourceManager();

        ValuesBucket valuesBucket = new ValuesBucket();
        valuesBucket.putString(AVStorage.Images.Media.DISPLAY_NAME, tmpImg);
        valuesBucket.putString("relative_path", "DCIM/");
        valuesBucket.putString(AVStorage.Images.Media.MIME_TYPE, "image/JPEG");

        valuesBucket.putInteger("is_pending", 1);
        DataAbilityHelper helper = DataAbilityHelper.creator(this);
        int id = helper.insert(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, valuesBucket);
        Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(id));
        FileDescriptor fd = helper.openFile(uri, "w");

        final PixelMap bitmapToSave = finalBitmap;

        class BackgroundRunnable implements Runnable{
            @Override
            public void run() {
                OutputStream os = null;
                try {
                    os = new FileOutputStream(fd);
                    ImagePacker imagePacker = ImagePacker.create();
                    ImagePacker.PackingOptions opts = new ImagePacker.PackingOptions();
                    opts.format = "image/jpeg";
                    opts.quality = OPT_QUALITY;
                    imagePacker.initializePacking(os, opts);
                    imagePacker.addImage(bitmapToSave);
                    imagePacker.finalizePacking();
                } finally {
                    if (os != null) {
                        try {
                            os.flush();
                            os.close();
                            valuesBucket.clear();
                            valuesBucket.putInteger("is_pending", 0);
                            helper.update(uri, valuesBucket, null);
                        } catch (IOException | DataAbilityRemoteException e) {
                            e.printStackTrace();
                        }
                    }

                    class UIRunnable implements Runnable {
                        @Override
                        public void run() {
                            btnDone.setEnabled(true);
                            btnSnap.setEnabled(true);
                            btnFlashMode.setEnabled(true);
                            progressBar.setVisibility(Component.HIDE);
                            btnDone.setVisibility(Component.HIDE);
                            btnFlashMode.setVisibility(Component.VISIBLE);
                            btnSnap.setVisibility(Component.VISIBLE);
                            isFirstImage = true;
                            bitmapsList.clear();

                            Intent resultIntent = new Intent();
                            setResult(resultIntent);
                            terminateAbility();

                            if (showPreview)
                            {
                                Intent ii = new Intent();
                                ii.setAction("action.previewlongimage");
                                startAbility(ii);
                            }

                        }
                    }

                    getContext().getUITaskDispatcher().syncDispatch(new UIRunnable());

                }
            }
        }
        getBackgroundHandler.postTask(new BackgroundRunnable());
    }




    private EventHandler getBackgroundHandler = new EventHandler(EventRunner.create("background"));

    public static void launch(Ability activity)
    {
        Intent ii = new Intent(activity.getIntent());
        activity.startAbilityForResult(ii, LONG_IMAGE_RESULT_CODE);
    }

    public static void launch(Ability activity, ImageMergeMode mergeMode)
    {
        Intent ii = new Intent(activity.getIntent());
        ii.setParam("mergeMode", mergeMode);
        activity.startAbilityForResult(ii, LONG_IMAGE_RESULT_CODE);
    }

    class LongCallback extends CameraView.Callback {

        @Override
        public void onCameraClosed(CameraView cameraView) {
            super.onCameraClosed(cameraView);
        }

        @Override
        public void onCameraOpened(CameraView cameraView) {
            super.onCameraOpened(cameraView);
        }

        @Override
        public void onPictureTaken(CameraView cameraView, byte[] data) {
            super.onPictureTaken(cameraView, data);

            if (isFirstImage)
            {
                imgRecent.setVisibility(Component.VISIBLE);
                btnDone.setVisibility(Component.VISIBLE);

                AnimatorProperty animatorProperty = cameraView.createAnimatorProperty();
                animatorProperty.moveFromY(0).moveToY((int)(cameraView.getHeight() * MOVE_RATIO_FIRST)).setDuration(MOVE_DURATION).setDelay(MOVE_DELAY_TIME);
                animatorProperty.start();
            }

            ImageSource.SourceOptions opts = new ImageSource.SourceOptions();
            ImageSource source = ImageSource.create(data,0,data.length, opts);

            ImageSource.DecodingOptions options = new ImageSource.DecodingOptions();
            options.editable = true;
            options.sampleSize = 2;

            PixelMap bitmap = source.createPixelmap(options);

            Rect rect = new Rect(0,0,bitmap.getImageInfo().size.width, bitmap.getImageInfo().size.height);

            PixelMap.InitializationOptions init = new PixelMap.InitializationOptions();

            PixelMap croppedBitmap = PixelMap.create(bitmap, rect, init);

            bitmapsList.add(croppedBitmap);

            imgRecent.setPixelMap(croppedBitmap);

            HiLog.info(LABEL, "btnSnapClick: ");

            AnimatorProperty animatorPropertyImgRecent = imgRecent.createAnimatorProperty();
            animatorPropertyImgRecent.moveFromY(0).moveToY((int)(-cameraView.getHeight() * MOVE_RATIO_SECOND)).setDuration(MOVE_DURATION).setDelay(0);
            animatorPropertyImgRecent.start();
        }
    }


}

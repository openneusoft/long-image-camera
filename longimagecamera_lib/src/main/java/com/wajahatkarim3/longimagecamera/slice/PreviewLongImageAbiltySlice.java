package com.wajahatkarim3.longimagecamera.slice;

import com.wajahatkarim3.longimagecamera.ResourceTable;
import com.wajahatkarim3.longimagecamera.cameraview.TouchImageView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.data.resultset.ResultSet;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;


public class PreviewLongImageAbiltySlice extends AbilitySlice {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 00201, PreviewLongImageAbiltySlice.class.getName());
    private TouchImageView imgLongPreview;
    private Image imgClose;
    private String imgstr;
    private Image imageFileName;

    private static final double SCREEN_WIDTH_DOUBLE = 512.0;
    private static final int SCREEN_WIDTH_INT = 512;
    @Override
    public final void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_preview_long_image);

        imgLongPreview = (TouchImageView) findComponentById(ResourceTable.Id_imgLongPreview);
        imgClose = new Image(getContext()); //test

        imgClose.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminateAbility();
            }
        });

        imgstr = intent.getStringParam("imageName");

        DataAbilityHelper helper = DataAbilityHelper.creator(this);
        try {
            ResultSet resultSet = helper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, new String[]{AVStorage.Images.Media.ID}, null);
            while (resultSet != null && resultSet.goToNextRow()) {
                int id = resultSet.getInt(resultSet.getColumnIndexForName(AVStorage.Images.Media.ID));
                Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(id));
                FileDescriptor fd = helper.openFile(uri, "r");

                ImageSource.SourceOptions opts = new ImageSource.SourceOptions();
                ImageSource imageSource = ImageSource.create(fd, opts);
                ImageSource.DecodingOptions options = new ImageSource.DecodingOptions();
                options.editable = true;
                options.sampleSize = 2;

                PixelMap d = imageSource.createPixelmap(options);
                int newHeight = (int) ( d.getImageInfo().size.height * (SCREEN_WIDTH_DOUBLE / d.getImageInfo().size.width) );
                Rect rect = new Rect(0,0,0,0);
                PixelMap.InitializationOptions init= new PixelMap.InitializationOptions();
                init.size = new Size(SCREEN_WIDTH_INT, newHeight);
                //init.size 512;
                PixelMap putImage = PixelMap.create(d, rect, init);
                imgLongPreview.setImageBitmap(putImage);
            }
        } catch (DataAbilityRemoteException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public final void onActive() {
        super.onActive();
    }

    @Override
    public final void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}

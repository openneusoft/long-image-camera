package com.wajahatkarim3.longimagecamera.cameraview;

import com.wajahatkarim3.longimagecamera.base.*;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.agp.graphics.SurfaceOps;
import ohos.agp.utils.Rect;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;
import ohos.bundle.AbilityInfo;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.camera.CameraKit;
import ohos.media.camera.device.*;
import ohos.media.camera.device.CameraInfo.FacingType;
import ohos.media.camera.params.Metadata;
import ohos.media.camera.params.PropertyKey;
import ohos.media.image.Image;
import ohos.media.image.ImageReceiver;
import ohos.media.image.common.ImageFormat;
import ohos.utils.PlainIntArray;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

public class CameraView extends StackLayout {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.DEBUG, 0x00201, CameraView.class.getName());
    // Default
    /**
     * Max preview width that is guaranteed by Camera2 API
     */
    private static final int MAX_PREVIEW_WIDTH = 2340;

    /**
     * Max preview height that is guaranteed by Camera2 API
     */
    private static final int MAX_PREVIEW_HEIGHT = 1080;

    private static final int DEGREE_CYCLE = 360;

    private static final int MAX_IMAGE = 5;

    private static final int FRAME_CONFIG_PREVIEW = 1;
    public static final int FLASH_OFF = 0;
    public static final int FLASH_ON = 1;
    public static final int FLASH_TORCH = 2;
    public static final int FLASH_AUTO = 3;
    public static final int FLASH_RED_EYE = 4;

    private static final int FACING_BACK = 0;
    private static final int FACING_FRONT = 1;

    private boolean mAdjustViewBounds;

    public CameraView(Context context) {
        this(context, null);
    }
    public CameraView(Context context, AttrSet attrSet) {
        this(context, attrSet, "CameraView");
    }

    public CameraView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mPreview = createPreviewImpl(context);

        addComponent(mPreview.getSurfaceProvider());
        WindowManager.getInstance().getTopWindow().get().setTransparent(true);
        mCameraKit = CameraKit.getInstance(context);
        if (mCameraKit == null) {
            new ToastDialog(context).setText("no camera");
        }

        setFacing(FACING_BACK);
        setAspectRatio(AspectRatio.of(2, 1));
        setAutoFocus(false);
        setFlash(FLASH_AUTO);
    }

    private CameraKit mCameraKit = null;
    private String mCameraId;
    private CameraAbility mCameraAbility;
    private int mFacing;
    private int mFlash;
    private int mHeight;
    private int mWidth;
    private int mDisplayOrientation;
    private static final PlainIntArray INTERNAL_FACINGS = new PlainIntArray();
    private final SizeMap mPreviewSizes = new SizeMap();
    private final SizeMap mPictureSizes = new SizeMap();
    private final PreviewImpl mPreview;
    private AspectRatio mAspectRatio = AspectRatio.of(2, 1);
    private ImageReceiver mImageReader;
    private boolean mAutoFocus;
    private FrameConfig.Builder mPreviewRequestBuilder;
    private final CallbackBridge mCallbacks = new CallbackBridge();
    private CameraStateCallbackImpl mCameraDeviceCallback = new CameraStateCallbackImpl();

    private Camera cameraDevice;
    private CameraConfig.Builder cameraConfigBuilder;

    /**
     * Open a camera device and start showing camera preview. This is typically called from
     * {@link Ability#onActive()}.
     */
    public void start() {
        startImpl();
    }

    /**
     * Stop camera preview and close the device. This is typically called from
     * {@link Ability#onInactive()}.
     */
    public void stop() {
        if (cameraDevice != null) {
            cameraDevice.release();
            cameraDevice = null;
        }
        if (mImageReader != null) {
            mImageReader.release();
            mImageReader = null;
        }
    }

    /**
     * @return {@code true} if the camera is opened.
     */
    public boolean isCameraOpened(){
        return cameraDevice != null;
    }

    /**
     * Add a new callback.
     *
     * @param callback The {@link Callback} to add.
     * @see #removeCallback(Callback)
     */
    public void addCallback(Callback callback){mCallbacks.add(callback);}

    /**
     * Remove a callback.
     *
     * @param callback The {@link Callback} to remove.
     * @see #addCallback(Callback)
     */
    public void removeCallback(Callback callback){mCallbacks.remove(callback);}

    /**
     * @param adjustViewBounds {@code true} if you want the CameraView to adjust its bounds to
     *                         preserve the aspect ratio of camera.
     * @see #getAdjustViewBounds()
     */
    public void setAdjustViewBounds(boolean adjustViewBounds) {
        if (mAdjustViewBounds != adjustViewBounds) {
            mAdjustViewBounds = adjustViewBounds;
            postLayout();
        }
    }

    /**
     * @return True when this CameraView is adjusting its bounds to preserve the aspect ratio of
     * camera.
     * @see #setAdjustViewBounds(boolean)
     */
    public boolean getAdjustViewBounds() {
        return mAdjustViewBounds;
    }

    /**
     * Chooses camera by the direction it faces.
     *
     * @param facing The camera facing. Must be either {@link #FACING_BACK} or
     *               {@link #FACING_FRONT}.
     */
    public void setFacing(int facing) {
        if (mFacing == facing) {
            return;
        }
        mFacing = facing;
        if (isCameraOpened()) {
            stop();
            start();
        }
    }

    /**
     * Gets the current flash mode.
     *
     * @return The current flash mode.
     */
    public int getFacing(){return mFacing;}

    /**
     * Gets all the aspect ratios supported by the current camera.
     */
    public Set<AspectRatio> getSupportedAspectRatios() {return mPreviewSizes.ratios();}

    /**
     * Sets the aspect ratio of camera.
     *
     * @param ratio The {@link AspectRatio} to be set.
     */
    public void setAspectRatio(AspectRatio ratio){
        if (setAspectRatioImpl(ratio)) {
            postLayout();
        }
    }

    /**
     * Gets the current aspect ratio of camera.
     *
     * @return The current {@link AspectRatio}. Can be {@code null} if no camera is opened yet.
     */
    public AspectRatio getAspectRatio() {return mAspectRatio;}

    /**
     * Enables or disables the continuous auto-focus mode. When the current camera doesn't support
     * auto-focus, calling this method will be ignored.
     *
     * @param autoFocus {@code true} to enable continuous auto-focus mode. {@code false} to
     *                  disable it.
     */
    public void setAutoFocus(boolean autoFocus){
        if (mAutoFocus == autoFocus) {
            return;
        }
        mAutoFocus = autoFocus;
        if (mPreviewRequestBuilder != null) {
            updateAutoFocus();
            if (cameraDevice != null) {
                try {
                    cameraDevice.stopLoopingCapture();
                    cameraDevice.triggerLoopingCapture(mPreviewRequestBuilder.build());
                } catch (IllegalStateException e) {
                    mAutoFocus = !mAutoFocus;
                }
            }
        }
    }

    /**
     * Returns whether the continuous auto-focus mode is enabled.
     *
     * @return {@code true} if the continuous auto-focus mode is enabled. {@code false} if it is
     * disabled, or if it is not supported by the current camera.
     */
    public boolean getAutoFocus() {return mAutoFocus;}

    /**
     * Sets the flash mode.
     *
     * @param flash The desired flash mode.
     */
    public void setFlash(int flash){
        if (mFlash == flash) {
            return;
        }
        int saved = mFlash;
        mFlash = flash;
        if (mPreviewRequestBuilder != null) {
            updateFlash();
            if (cameraDevice != null) {
                try {
                    cameraDevice.stopLoopingCapture();
                    cameraDevice.triggerLoopingCapture(mPreviewRequestBuilder.build());
                } catch (IllegalStateException e) {
                    mFlash = saved; // Revert
                }
            }
        }
    }

    /**
     * Gets the current flash mode.
     *
     * @return The current flash mode.
     */
    public int getFlash(){return mFlash;}

    /**
     * Take a picture. The result will be returned to
     * {@link Callback#onPictureTaken(CameraView, byte[])}.
     */
    public void takePicture(){
        if (mAutoFocus) {
            lockFocus();
        } else {
            captureStillPicture();
        }
    }

    private boolean startImpl(){
        if (!chooseCameraIdByFacing()) {
            return false;
        }
        collectCameraInfo();
        prepareImageReader();

        mPreview.getSurfaceOps().addCallback(new TextureSurfaceCallback());
        HiLog.info(LABEL, "end");

        return true;
    }

    /**
     * <p>Chooses a camera ID by the specified camera facing ({@link #mFacing}).</p>
     * <p>This rewrites {@link #mCameraId}, {@link #mCameraAbility}, and optionally
     * {@link #mFacing}.</p>
     */
    private boolean chooseCameraIdByFacing() {
        try {
            final String[] ids = mCameraKit.getCameraIds();
            if (ids.length == 0) { // No camera
                throw new RuntimeException("No camera available.");
            }
            for (String id : ids) {
                CameraAbility cameraAbility = mCameraKit.getCameraAbility(id);
                int internal = cameraAbility.getPropertyValue(PropertyKey.SENSOR_ORIENTATION);
                if (internal == mCameraKit.getCameraInfo(id).getFacingType()) {
                    mCameraId = id;
                    mCameraAbility = cameraAbility;
                    return true;
                }
            }
            mCameraId = ids[0];
            mCameraAbility = mCameraKit.getCameraAbility(mCameraId);
            CameraInfo cameraInfo = mCameraKit.getCameraInfo(mCameraId);
            Integer internal = cameraInfo.getFacingType();
            if (internal == null) {
                throw new NullPointerException("Unexpected state: LENS_FACING null");
            }

            for (int i = 0, count = INTERNAL_FACINGS.size(); i < count; i++) {
                if (INTERNAL_FACINGS.valueAt(i) == internal) {
                    mFacing = INTERNAL_FACINGS.keyAt(i);
                    return true;
                }
            }
            // The operation can reach here when the only camera device is an external one.
            // We treat it as facing back.
            mFacing = FacingType.CAMERA_FACING_BACK;
            return true;
        } catch (IllegalStateException  e) {
            throw new RuntimeException("Failed to get a list of camera devices");
        }
    }

    /**
     * <p>Collects some information from {@link #mCameraAbility}.</p>
     * <p>This rewrites {@link #mPreviewSizes}, {@link #mPictureSizes}, and optionally,
     * {@link #mAspectRatio}.</p>
     */
    private void collectCameraInfo() {
        mPreviewSizes.clear();
        List<Size> sizeList = mCameraAbility.getSupportedSizes(mPreview.getOutputClass());
        for (ohos.media.image.common.Size size : mCameraAbility.getSupportedSizes(ImageFormat.JPEG)) {
            int width = size.width;
            int height = size.height;
            if (width <= MAX_PREVIEW_WIDTH && height <= MAX_PREVIEW_HEIGHT) {
                mPreviewSizes.add(new Size(width, height));
            }
        }
        mPictureSizes.clear();
        collectPictureSizes(mPictureSizes, mCameraAbility);
        for (AspectRatio ratio : mPreviewSizes.ratios()) {
            if (!mPictureSizes.ratios().contains(ratio)) {
                mPreviewSizes.remove(ratio);
            }
        }

        if (!mPreviewSizes.ratios().contains(mAspectRatio)) {
            mAspectRatio = mPreviewSizes.ratios().iterator().next();
        }
    }

    private void prepareImageReader() {
        Size largest = mPreviewSizes.sizes(mAspectRatio).last();
        mImageReader = ImageReceiver.create(largest.getWidth(), largest.getHeight(),
                ImageFormat.JPEG, /* maxImages */ MAX_IMAGE);
        mImageReader.setImageArrivalListener(mOnImageAvailableListener);
    }


    protected final void collectPictureSizes(SizeMap sizes, CameraAbility cameraAbility) {
        for (ohos.media.image.common.Size size : cameraAbility.getSupportedSizes(ImageFormat.JPEG)) {
            mPictureSizes.add(new Size(size.width, size.height));
        }
    }

    private PreviewImpl createPreviewImpl(Context context){
        PreviewImpl preview;
        preview = new TextureViewPreview(context);
        return preview;
    }

    private Component getViewImpl(){
        return mPreview.getView();
    }

    /**
     * <p>Starts opening a camera device.</p>
     * <p>The result will be processed in {@link #mCameraDeviceCallback}.</p>
     */
    private void startOpeningCamera() {
        try {
            EventHandler eventHandler = new EventHandler(EventRunner.create("LocalCamera"));
            mCameraKit.createCamera(mCameraId, mCameraDeviceCallback, eventHandler);
            HiLog.info(LABEL, "startOpeningCamera");
        } catch (IllegalStateException e) {
            throw new RuntimeException("Failed to open camera: " + mCameraId, e);
        }
    }

    private boolean setAspectRatioImpl(AspectRatio ratio) {
        if (ratio == null || ratio.equals(mAspectRatio) ||
                !mPreviewSizes.ratios().contains(ratio)) {
            // TODO: Better error handling
            return false;
        }
        mAspectRatio = ratio;
        if (cameraDevice != null) {
            cameraDevice.release();
            cameraDevice = null;
            startCaptureSession();
        }

        return true;
    }

    public final void setDisplayOrientation(AbilityInfo.DisplayOrientation displayOrientation)
    {
        if(displayOrientation == AbilityInfo.DisplayOrientation.LANDSCAPE)
        {
            mHeight = getHeight();
            mWidth = getWidth();

            Size size = new Size(mHeight, mWidth);
            mPreview.setDisplayOrientation(size);
        } else if (displayOrientation == AbilityInfo.DisplayOrientation.PORTRAIT)
        {
            Size size = new Size(mWidth, mHeight);
            mPreview.setDisplayOrientation(size);
        }

    }

    /**
     * <p>Starts a capture session for camera preview.</p>
     * <p>This rewrites {@link #mPreviewRequestBuilder}.</p>
     * <p>The result will be continuously processed in {@link #mSessionCallback}.</p>
     */
    void startCaptureSession() {
        if (!isCameraOpened() || !mPreview.isReady() || mImageReader == null) {
            return;
        }
        Size previewSize = chooseOptimalSize();
        mPreview.setBufferSize(previewSize.getWidth(), previewSize.getHeight());

        try {
            mPreviewRequestBuilder = cameraDevice.getFrameConfigBuilder(FRAME_CONFIG_PREVIEW);
            mPreviewRequestBuilder.addSurface(mPreview.getSurfaceOps().getSurface());
            cameraDevice.configure(cameraDevice.getCameraConfigBuilder().build());

        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Failed to start camera session");
        }
    }

    /**
     * Updates the internal state of auto-focus to {@link #mAutoFocus}.
     */
    void updateAutoFocus() {
        Rect rect = new Rect();

        if (mAutoFocus) {
            int modes = mPreviewRequestBuilder.getAfMode();
            // Auto focus is not supported
            if ( modes == Metadata.AfMode.AF_MODE_OFF) {
                mAutoFocus = false;
                mPreviewRequestBuilder.setAfMode(Metadata.AfMode.AF_MODE_OFF,
                        rect);
            } else {
                mPreviewRequestBuilder.setAfMode(Metadata.AfMode.AF_MODE_CONTINUOUS,
                        rect);
            }
        } else {
            mPreviewRequestBuilder.setAfMode(Metadata.AfMode.AF_MODE_OFF,
                    rect);
        }
    }

    /**
     * Updates the internal state of flash to {@link #mFlash}.
     */
    void updateFlash() {
        Rect rect = new Rect();
        switch (mFlash) {
            case FLASH_OFF:
                mPreviewRequestBuilder.setAeMode(Metadata.AeMode.AE_MODE_OFF,
                        rect);
                mPreviewRequestBuilder.setFlashMode(Metadata.FlashMode.FLASH_CLOSE);
                break;
            case FLASH_ON:
                mPreviewRequestBuilder.setAeMode(Metadata.AeMode.AE_MODE_ON,
                        rect);
                mPreviewRequestBuilder.setFlashMode(Metadata.FlashMode.FLASH_ALWAYS_OPEN);
                break;
            case FLASH_TORCH:
                mPreviewRequestBuilder.setAeMode(Metadata.AeMode.AE_MODE_ON,
                        rect);
                mPreviewRequestBuilder.setFlashMode(Metadata.FlashMode.FLASH_AUTO);
                break;
            case FLASH_AUTO:
                mPreviewRequestBuilder.setFlashMode(Metadata.FlashMode.FLASH_AUTO);
                break;
            case FLASH_RED_EYE:
                mPreviewRequestBuilder.setFlashMode(Metadata.FlashMode.FLASH_CLOSE);
                break;
        }
    }

    /**
     * Locks the focus as the first step for a still image capture.
     */
    private void lockFocus() {
        mPreviewRequestBuilder.setAfTrigger(Metadata.AfTrigger.AF_TRIGGER_START);
        try {
            cameraDevice.getCameraConfigBuilder().setRunningMode(Metadata.AfMode.AF_MODE_TOUCH_LOCK);
            cameraDevice.configure(cameraDevice.getCameraConfigBuilder().build());
            cameraDevice.stopLoopingCapture();
            cameraDevice.triggerSingleCapture(mPreviewRequestBuilder.build());
        } catch (IllegalArgumentException e) {
            HiLog.error(LABEL, "Failed to lock focus.", e);
        }
    }

    /**
     * Captures a still picture.
     */
    void captureStillPicture() {
        try {
            mPreviewRequestBuilder = cameraDevice.getFrameConfigBuilder(Camera.FrameConfigType.FRAME_CONFIG_PICTURE);
            mPreviewRequestBuilder.addSurface(mImageReader.getRecevingSurface());

            Rect rect = new Rect();
            mPreviewRequestBuilder.setAfMode(cameraDevice.getFrameConfigBuilder(FRAME_CONFIG_PREVIEW).getAfMode(),rect);

            switch (mFlash) {
                case FLASH_OFF:
                    mPreviewRequestBuilder.setAeMode(Metadata.AeMode.AE_MODE_OFF,
                            rect);
                    mPreviewRequestBuilder.setFlashMode(Metadata.FlashMode.FLASH_CLOSE);
                    break;
                case FLASH_ON:
                    mPreviewRequestBuilder.setAeMode(Metadata.AeMode.AE_MODE_ON,
                            rect);
                    mPreviewRequestBuilder.setFlashMode(Metadata.FlashMode.FLASH_CLOSE);
                    break;
                case FLASH_TORCH:
                    mPreviewRequestBuilder.setAeMode(Metadata.AeMode.AE_MODE_ON,
                            rect);
                    mPreviewRequestBuilder.setFlashMode(Metadata.FlashMode.FLASH_AUTO);
                    break;
                case FLASH_AUTO:
                    mPreviewRequestBuilder.setFlashMode(Metadata.FlashMode.FLASH_AUTO);
                    break;
                case FLASH_RED_EYE:
                    mPreviewRequestBuilder.setFlashMode(Metadata.FlashMode.FLASH_CLOSE);
                    break;
            }
            // Calculate JPEG orientation.
            @SuppressWarnings("ConstantConditions")
            int sensorOrientation = mCameraAbility.getPropertyValue(PropertyKey.SENSOR_ORIENTATION);
            int sen = (sensorOrientation +
                    mDisplayOrientation * (mFacing == FACING_FRONT ? 1 : -1) +
                    DEGREE_CYCLE) % DEGREE_CYCLE;

            mPreviewRequestBuilder.setImageRotation(sen);

            // Stop preview and capture a still picture.
            cameraDevice.triggerSingleCapture(mPreviewRequestBuilder.build());

        } catch (IllegalStateException e) {
            HiLog.error(LABEL, "Cannot capture a still picture.");
        }
    }

    /**
     * Chooses the optimal preview size based on {@link #mPreviewSizes} and the surface size.
     *
     * @return The picked size for camera preview.
     */
    private Size chooseOptimalSize() {
        int surfaceLonger, surfaceShorter;
        final int surfaceWidth = mPreview.getWidth();
        final int surfaceHeight = mPreview.getHeight();
        if (surfaceWidth < surfaceHeight) {
            surfaceLonger = surfaceHeight;
            surfaceShorter = surfaceWidth;
        } else {
            surfaceLonger = surfaceWidth;
            surfaceShorter = surfaceHeight;
        }
        SortedSet<Size> candidates = mPreviewSizes.sizes(mAspectRatio);

        // Pick the smallest of those big enough
        for (Size size : candidates) {
            if (size.getWidth() >= surfaceLonger && size.getHeight() >= surfaceShorter) {
                return size;
            }
        }
        // If no size is big enough, pick the largest one.
        return candidates.last();
    }

    private final ImageReceiver.IImageArrivalListener mOnImageAvailableListener
            = new ImageReceiver.IImageArrivalListener() {
        @Override
        public void onImageArrival(ImageReceiver imageReceiver) {
            ImageSaver imageSaver = new ImageSaver(imageReceiver);
            getContext().getUITaskDispatcher().syncDispatch(imageSaver);
        }
    };

    /**
     * Callback for monitoring events about {@link CameraView}.
     */
    @SuppressWarnings("UnusedParameters")

    private FrameStateCallback frameStateCallbackImpl = new FrameStateCallback(){
        @Override
        public void onFrameStarted(Camera camera, FrameConfig frameConfig, long frameNumber, long timestamp) {}
        @Override
        public void onFrameProgressed(Camera camera, FrameConfig frameConfig, FrameResult frameResult) {}
        @Override
        public void onFrameFinished(Camera camera, FrameConfig frameConfig, FrameResult frameResult) {}
        @Override
        public void onFrameError(Camera camera, FrameConfig frameConfig, int errorCode, FrameResult frameResult) {}
        @Override
        public void onCaptureTriggerStarted(Camera camera, int captureTriggerId, long firstFrameNumber) {}
        @Override
        public void onCaptureTriggerFinished(Camera camera, int captureTriggerId, long lastFrameNumber) {}
        @Override
        public void onCaptureTriggerInterrupted(Camera camera, int captureTriggerId) {}
    };

    private final class CameraStateCallbackImpl extends CameraStateCallback {
        @Override
        public void onCreated(Camera camera) {
            cameraDevice = camera;
            cameraConfigBuilder = camera.getCameraConfigBuilder();
            if (cameraConfigBuilder == null) {
                return;
            }
            cameraConfigBuilder.addSurface(mPreview.getSurfaceOps().getSurface());

            cameraConfigBuilder.addSurface(mImageReader.getRecevingSurface());

            cameraConfigBuilder.setFrameStateCallback(frameStateCallbackImpl, new EventHandler(EventRunner.create("FrameCamera")));
            try {
                camera.configure(cameraConfigBuilder.build());
            } catch (IllegalArgumentException e) {
                HiLog.error(LABEL, "Argument Exception");
            } catch (IllegalStateException e) {
                HiLog.error(LABEL, "State Exception");
            }
        }

        @Override
        public void onConfigured(Camera camera){
            mPreviewRequestBuilder = camera.getFrameConfigBuilder(Camera.FrameConfigType.FRAME_CONFIG_PREVIEW);
            mPreviewRequestBuilder.addSurface(mPreview.getSurfaceOps().getSurface());
            FrameConfig previewFrameConfig = mPreviewRequestBuilder.build();
            Integer triggerId;
            try {
                triggerId = camera.triggerLoopingCapture(previewFrameConfig);
            } catch (IllegalArgumentException e) {
                HiLog.error(LABEL, "Argument Exception");
            } catch (IllegalStateException e) {
                HiLog.error(LABEL, "State Exception");
            }
        }

        @Override
        public void onCreateFailed(String cameraId, int errorCode){
            HiLog.info(LABEL, "--------------------相机创建失败！相机id: %{public}s; 失败编码: %{public}s;", cameraId, errorCode);

        }
    }

    class ImageSaver implements Runnable {
        private final ImageReceiver myImageReceiver;

        ImageSaver(ImageReceiver imageReceiver) {
            myImageReceiver = imageReceiver;
        }

        @Override
        public void run() {
            try {
                Image image = myImageReceiver.readNextImage();
                Image.Component planes = image.getComponent(ImageFormat.ComponentType.JPEG);
                ByteBuffer buffer = planes.getBuffer();
                byte[] data = new byte[buffer.remaining()];
                buffer.get(data);
                mCallbacks.onPictureTaken(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class TextureSurfaceCallback implements SurfaceOps.Callback {

        @Override
        public void surfaceCreated(SurfaceOps surfaceOps) {
            surfaceOps.setFixedSize(mPreviewSizes.sizes(mAspectRatio).last().getWidth(), mPreviewSizes.sizes(mAspectRatio).last().getHeight());
            HiLog.info(LABEL, "surfaceCreated");
            new Thread(){
                @Override
                public void run(){
                    startOpeningCamera();
                }
            }.start();
        }

        @Override
        public void surfaceChanged(SurfaceOps surfaceOps, int i, int i1, int i2) {
        }

        @Override
        public void surfaceDestroyed(SurfaceOps surfaceOps) {

        }
    }

    private class CallbackBridge {
        private final ArrayList<Callback> callbacks = new ArrayList<>();

        private boolean mRequestLayoutOnOpen;

        CallbackBridge() {
        }

        public void add(Callback callback) {
            callbacks.add(callback);
        }

        public void remove(Callback callback) {
            callbacks.remove(callback);
        }

        public void onCameraOpened() {
            if (mRequestLayoutOnOpen) {
                mRequestLayoutOnOpen = false;
                postLayout();
            }
            for (Callback callback : callbacks) {
                callback.onCameraOpened(CameraView.this);
            }
        }

        public void onCameraClosed() {
            for (Callback callback : callbacks) {
                callback.onCameraClosed(CameraView.this);
            }
        }

        public void onPictureTaken(byte[] data) {
            for (Callback callback : callbacks) {
                callback.onPictureTaken(CameraView.this, data);
            }
        }

        public void reserveRequestLayoutOnOpen() {
            mRequestLayoutOnOpen = true;
        }
    }

    public abstract static class Callback {
        /**
         * Called when camera is opened.
         *
         * @param cameraView The associated {@link CameraView}.
         */
        public void onCameraOpened(CameraView cameraView) {
        }

        /**
         * Called when camera is closed.
         *
         * @param cameraView The associated {@link CameraView}.
         */
        public void onCameraClosed(CameraView cameraView) {
        }

        /**
         * Called when a picture is taken.
         *
         * @param cameraView The associated {@link CameraView}.
         * @param data       JPEG data.
         */
        public void onPictureTaken(CameraView cameraView, byte[] data) {
        }
    }

}


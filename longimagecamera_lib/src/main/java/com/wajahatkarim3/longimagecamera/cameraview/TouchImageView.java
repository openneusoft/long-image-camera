package com.wajahatkarim3.longimagecamera.cameraview;

import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.render3d.math.Quaternion;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.global.configuration.Configuration;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Rect;
import ohos.multimodalinput.event.TouchEvent;

import static java.lang.StrictMath.abs;

public class TouchImageView extends Image {

    private static final String DEBUG = "DEBUG";

    //
    // SuperMin and SuperMax multipliers. Determine how much the image can be
    // zoomed below or above the zoom boundaries, before animating back to the
    // min/max zoom boundary.
    //
    private static final float SUPER_MIN_MULTIPLIER = .75f;
    private static final float SUPER_MAX_MULTIPLIER = 1.25f;

    private static final float HELF_RATIO = 0.5f;

    private static final int ARRAY_LENGTH = 9;
    private static final int MIN_SCALE = 1;
    private static final int MAX_SCALE = 6;
    private static final int VIEW_WIDTH = 1059;
    private static final int VIEW_HEIGTH = 1835;

    private static final int NUMBER_TWO = 2;
    private static final int NUMBER_FOUR = 4;
    private static final int NUMBER_FIVE = 5;
    private static final int NUMBER_NINE = 9;

    private static final int FIX_LENGTH = 200;

    private static final int UNITS = 1000;
    private static final int MAX_VX_VELOCITY = 4000;
    private static final int MAX_VY_VELOCITY = 4000;

    private static final int VX_VELOCITY = 3000;
    private static final int VY_VELOCITY = 3000;

    private static final float MIN_DISTANCE = 300f;

    private static final int MIN_THRESHOLD = 150;
    private static final int MIN_DELTA_TIME = 40;
    private static final int MAX_DELTA_TIME = 100;
    private static final int FIX_SCALE = 50;
    //
    // Scale of image ranges from minScale to maxScale, where minScale == 1
    // when the image is stretched to fit view.
    //
    private float normalizedScale;

    //
    // Matrix applied to image. MSCALE_X and MSCALE_Y should always be equal.
    // MTRANS_X and MTRANS_Y are the other values used. prevMatrix is the matrix
    // saved prior to the screen rotating.
    //
    private Matrix matrix, prevMatrix;
    private Element element, preElement;
    private enum State { NONE, DRAG, ZOOM, FLING, ANIMATE_ZOOM };
    private State state;

    private float minScale;
    private float maxScale;
    private float superMinScale;
    private float superMaxScale;
    private float[] m;

    private Context context;
    private Fling fling;

    private ScaleMode mScaleType;

    private boolean imageRenderedAtLeastOnce;
    private boolean onDrawReady;
    private Paint paint;
    private ZoomVariables delayedZoomVariables;
    private long previousUpEventTime;
    private PixelMap originPixelMap;
    private VelocityDetector mVelocityDetector;
    private TouchEvent mCurrentDownEvent;
    private boolean stop = false;
    //
    // Size of view and previous view size (ie before rotation)
    //
    private int viewWidth, viewHeight, prevViewWidth, prevViewHeight;

    //
    // Size of image when it is stretched to fit view. Before and After rotation.
    //
    private float matchViewWidth, matchViewHeight, prevMatchViewWidth, prevMatchViewHeight;
    private TaskDispatcher uIthread = null;

    private TouchEventListener userTouchListener = null;
    private OnTouchImageViewListener touchImageViewListener = null;

    public TouchImageView(Context contextTemp) {
        super(contextTemp);
        sharedConstructing(contextTemp);
    }

    public TouchImageView(Context contextTemp, AttrSet attrs) {
        super(contextTemp, attrs);
        sharedConstructing(contextTemp);
    }

    public TouchImageView(Context contextTemp, AttrSet attrs, String defStyle) {
        super(contextTemp, attrs, defStyle);
        sharedConstructing(contextTemp);
    }

    private void sharedConstructing(Context contextTemp) {
        super.setClickable(true);
        paint = new Paint();
        this.context = contextTemp;
        matrix = new Matrix();
        prevMatrix = new Matrix();
        m = new float[ARRAY_LENGTH];
        normalizedScale = 1;
        if (mScaleType == null) {
            mScaleType = ScaleMode.CENTER;
        }
        minScale = MIN_SCALE;
        maxScale = MAX_SCALE;
        superMinScale = SUPER_MIN_MULTIPLIER * minScale;
        superMaxScale = SUPER_MAX_MULTIPLIER * maxScale;
        super.setTouchEventListener(new PrivateOnTouchListener());
        super.setScaledListener(new PrivateOnScaleListener());
        super.setEstimateSizeListener(new privateEstimateSizeListener());
        super.setScaleMode(ScaleMode.CENTER);
        addDrawTask(mDrawTask);
        viewWidth = VIEW_WIDTH;
        viewHeight = VIEW_HEIGTH;
        invalidate();

        setState(State.NONE);
        uIthread = contextTemp.getUITaskDispatcher();
        onDrawReady = false;
    }

    private DrawTask mDrawTask = new DrawTask(){
        @Override
        public void onDraw(Component component, Canvas canvas) {
            onDrawReady = true;
            imageRenderedAtLeastOnce = true;

            if (delayedZoomVariables != null) {
                setZoom(delayedZoomVariables.scale, delayedZoomVariables.focusX, delayedZoomVariables.focusY, delayedZoomVariables.scaleType);
                delayedZoomVariables = null;
            }
            Paint p = new Paint();
            p.setAntiAlias(true);
            p.setStyle(Paint.Style.FILL_STYLE);
            p.setColor(Color.WHITE);
            canvas.drawRect(0,0,component.getWidth(),component.getHeight(), p);
            canvas.setMatrix(matrix);
            canvas.drawPixelMapHolder(new PixelMapHolder(originPixelMap),0,0,paint);
        }
    };

    @Override
    public final void setTouchEventListener(TouchEventListener l) {
        userTouchListener = l;
    }

    public final void setOnTouchImageViewListener(OnTouchImageViewListener l) {
        touchImageViewListener = l;
    }

    public final void setImageBitmap(PixelMap bm) {
        super.setPixelMap(bm);
        originPixelMap = bm;
        savePreviousImageValues();
        fitImageToView();
    }    //setpixmap

    @Override
    public final void setScaleMode(ScaleMode type) {
        if (type == ScaleMode.ZOOM_START || type == ScaleMode.ZOOM_END) {
            throw new UnsupportedOperationException("TouchImageView does not support FIT_START or FIT_END");
        }
        mScaleType = type;
        if (onDrawReady) {
            //
            // If the image is already rendered, scaleType has been called programmatically
            // and the TouchImageView should be updated with the new scaleType.
            //
            setZoom(this);

        }
    }

    @Override
    public final ScaleMode getScaleMode() {
        return mScaleType;
    } //imageview

    /**
     * Returns false if image is in initial, unzoomed state. False, otherwise.
     * @return true if image is zoomed
     */
    public boolean isZoomed() {
        return normalizedScale != 1;
    }

    /**
     * Return a Rect representing the zoomed image.
     * @return rect representing zoomed image
     */
    public Rect getZoomedRect() {
        if (mScaleType == ScaleMode.STRETCH) {
            throw new UnsupportedOperationException("getZoomedRect() not supported with FIT_XY");
        }

        Quaternion topLeft = transformCoordTouchToBitmap(0, 0, true);
        Quaternion bottomRight = transformCoordTouchToBitmap(viewWidth, viewHeight, true);

        int w = getPixelMap().getImageInfo().size.width; //imageview
        int h = getPixelMap().getImageInfo().size.height;//imageview
        return new Rect((int)topLeft.getX() / w, (int)topLeft.getY() / h, (int)bottomRight.getX() / w, (int)bottomRight.getY() / h);
    }

    /**
     * Save the current matrix and view dimensions
     * in the prevMatrix and prevView variables.
     */
    private void savePreviousImageValues() {
        if (matrix != null && viewHeight != 0 && viewWidth != 0) {
            matrix.getElements(m);
            prevMatrix.setElements(m);
            prevMatchViewHeight = matchViewHeight;
            prevMatchViewWidth = matchViewWidth;
            prevViewHeight = viewHeight;
            prevViewWidth = viewWidth;
        }
    }

    @Override
    public final void onAttributeConfigChanged(Configuration newConfig) {
        super.onAttributeConfigChanged(newConfig);
        savePreviousImageValues();
    }

    /**
     * Get the max zoom multiplier.
     * @return max zoom multiplier.
     */
    public float getMaxZoom() {
        return maxScale;
    }

    /**
     * Set the max zoom multiplier. Default value: 3.
     * @param max max zoom multiplier.
     */
    public void setMaxZoom(float max) {
        maxScale = max;
        superMaxScale = SUPER_MAX_MULTIPLIER * maxScale;
    }

    /**
     * Get the min zoom multiplier.
     * @return min zoom multiplier.
     */
    public float getMinZoom() {
        return minScale;
    }

    /**
     * Get the current zoom. This is the zoom relative to the initial
     * scale, not the original resource.
     * @return current zoom multiplier.
     */
    public float getCurrentZoom() {
        return normalizedScale;
    }

    /**
     * Set the min zoom multiplier. Default value: 1.
     * @param min min zoom multiplier.
     */
    public void setMinZoom(float min) {
        minScale = min;
        superMinScale = SUPER_MIN_MULTIPLIER * minScale;
    }

    /**
     * Reset zoom and translation to initial state.
     */
    public void resetZoom() {
        normalizedScale = 1;
        fitImageToView();
    }

    /**
     * Set zoom to the specified scale. Image will be centered by default.
     * @param scale
     */
    public void setZoom(float scale) {
        setZoom(scale, HELF_RATIO, HELF_RATIO);
    }

    /**
     * Set zoom to the specified scale. Image will be centered around the point
     * (focusX, focusY). These floats range from 0 to 1 and denote the focus point
     * as a fraction from the left and top of the view. For example, the top left
     * corner of the image would be (0, 0). And the bottom right corner would be (1, 1).
     * @param scale
     * @param focusX
     * @param focusY
     */
    public void setZoom(float scale, float focusX, float focusY) {
        setZoom(scale, focusX, focusY, mScaleType);
    }

    /**
     * Set zoom to the specified scale. Image will be centered around the point
     * (focusX, focusY). These floats range from 0 to 1 and denote the focus point
     * as a fraction from the left and top of the view. For example, the top left
     * corner of the image would be (0, 0). And the bottom right corner would be (1, 1).
     * @param scale
     * @param focusX
     * @param focusY
     * @param scaleType
     */
    public void setZoom(float scale, float focusX, float focusY, ScaleMode scaleType) {
        //
        // setZoom can be called before the image is on the screen, but at this point,
        // image and view sizes have not yet been calculated in onMeasure. Thus, we should
        // delay calling setZoom until the view has been measured.
        //
        if (!onDrawReady) {
            delayedZoomVariables = new ZoomVariables(scale, focusX, focusY, scaleType);
            return;
        }

        if (scaleType != mScaleType) {
            setScaleMode(scaleType);
        }
        resetZoom();
        scaleImage(scale, viewWidth / 2, viewHeight / 2, true);
        matrix.getElements(m);
        m[NUMBER_TWO] = -((focusX * getImageWidth()) - (viewWidth * HELF_RATIO));
        m[NUMBER_FIVE] = -((focusY * getImageHeight()) - (viewHeight * HELF_RATIO));
        matrix.setElements(m);
        fixTrans();
        invalidate();
    }

    /**
     * Set zoom parameters equal to another TouchImageView. Including scale, position,
     * and ScaleType.
     *  TouchImageView
     */
    public void setZoom(TouchImageView img) {
        Quaternion center = img.getScrollPosition();
        setZoom(img.getCurrentZoom(), center.getX(), center.getY(), img.getScaleMode());
    }

    /**
     * Return the point at the center of the zoomed image. The PointF coordinates range
     * in value between 0 and 1 and the focus point is denoted as a fraction from the left
     * and top of the view. For example, the top left corner of the image would be (0, 0).
     * And the bottom right corner would be (1, 1).
     * @return PointF representing the scroll position of the zoomed image.
     */
    public Quaternion getScrollPosition() {
        PixelMap drawable = getPixelMap();
        if (drawable == null) {
            return null;
        }
        int drawableWidth = drawable.getImageInfo().size.width;
        int drawableHeight = drawable.getImageInfo().size.height;

        Quaternion point = transformCoordTouchToBitmap(viewWidth / 2, viewHeight / 2, true);

        return new Quaternion(point.getX()/drawableWidth, point.getY()/drawableHeight, 0, 0);
    }

    /**
     * Set the focus point of the zoomed image. The focus points are denoted as a fraction from the
     * left and top of the view. The focus points can range in value between 0 and 1.
     * @param focusX
     * @param focusY
     */
    public void setScrollPosition(float focusX, float focusY) {
        setZoom(normalizedScale, focusX, focusY);
    }

    /**
     * Performs boundary checking and fixes the image matrix if it
     * is out of bounds.
     */
    private void fixTrans() {
        matrix.getElements(m);
        float transX = m[NUMBER_TWO];  //Matrix.MTRANS_X
        float transY = m[NUMBER_FIVE] -FIX_LENGTH;  //Matrix.MTRANS_Y

        float fixTransX = getFixTrans(transX, viewWidth, getImageWidth());
        float fixTransY = getFixTrans(transY, viewHeight, getImageHeight());

        if (fixTransX != 0 || fixTransY != 0) {
            matrix.postTranslate(fixTransX, fixTransY);
        }
    }

    private void fixTran() {
        matrix.getElements(m);
        float transX = m[NUMBER_TWO];  //Matrix.MTRANS_X
        float transY = m[NUMBER_FIVE] -FIX_LENGTH;  //Matrix.MTRANS_Y

        float fixTransX = getFixTrans(transX, viewWidth, getImageWidth());
        float fixTransY = getFixTrans(transY, viewHeight, getImageHeight());
        if(getImageWidth() < viewWidth){
            if (fixTransY != 0) {
                matrix.postTranslate(fixTransX, fixTransY);
                stop = true;
            }
        } else{
            if(fixTransX != 0 || fixTransY != 0) {
                matrix.postTranslate(fixTransX, fixTransY);
                stop = true;
            }
        }

    }

    /**
     * When transitioning from zooming from focus to zoom from center (or vice versa)
     * the image can become unaligned within the view. This is apparent when zooming
     * quickly. When the content size is less than the view size, the content will often
     * be centered incorrectly within the view. fixScaleTrans first calls fixTrans() and
     * then makes sure the image is centered correctly within the view.
     */
    private void fixScaleTrans() {
        fixTrans();
        matrix.getElements(m);
        if (getImageWidth() < viewWidth) {
            m[2] = (viewWidth - getImageWidth()) / 2;
        }

        if (getImageHeight() < viewHeight) {
            m[NUMBER_FIVE] = (viewHeight - getImageHeight()) / 2;
        }
        matrix.setElements(m);
    }

    private float getFixTrans(float trans, float viewSize, float contentSize) {
        float minTrans, maxTrans;

        if (contentSize <= viewSize) {
            minTrans = 0;
            maxTrans = viewSize - contentSize;

        } else {
            minTrans = viewSize - contentSize;
            maxTrans = 0;
        }

        if (trans < minTrans)
            return -trans + minTrans;
        if (trans > maxTrans)
            return -trans + maxTrans;
        return 0;
    }

    private float getFixDragTrans(float delta, float viewSize, float contentSize) {
        if (contentSize <= viewSize) {
            return 0;
        }
        return delta;
    }

    private float getImageWidth() {
        return matchViewWidth * normalizedScale;
    }

    private float getImageHeight() {
        return matchViewHeight * normalizedScale;
    }

    /**
     * If the normalizedScale is equal to 1, then the image is made to fit the screen. Otherwise,
     * it is made to fit the screen according to the dimensions of the previous image matrix. This
     * allows the image to maintain its zoom after rotation.
     */
    private void fitImageToView() {
        PixelMap drawable = getPixelMap(); //imageview
        if (drawable == null || drawable.getImageInfo().size.width == 0 || drawable.getImageInfo().size.height == 0) {
            return;
        }
        if (matrix == null || prevMatrix == null) {
            return;
        }

        int drawableWidth = drawable.getImageInfo().size.width;
        int drawableHeight = drawable.getImageInfo().size.height;

        //
        // Scale image for view
        //
        float scaleX = (float) viewWidth / drawableWidth;
        float scaleY = (float) viewHeight / drawableHeight;

        switch (mScaleType) {     //imageview
            case CENTER:
                scaleX = 1;
                scaleY = 1;
                break;

            case CLIP_CENTER:
                scaleX = Math.max(scaleX, scaleY);
                scaleY = scaleX;
                break;

            case INSIDE:
                scaleX = Math.min(1, Math.min(scaleX, scaleY));
                scaleY = scaleX;
                break;
            case ZOOM_CENTER:
                scaleX = Math.min(scaleX, scaleY);
                scaleY = scaleX;
                break;
            case STRETCH:
                break;

            default:
                //
                // FIT_START and FIT_END not supported
                //
                throw new UnsupportedOperationException("TouchImageView does not support FIT_START or FIT_END");

        }

        //
        // Center the image
        //
        float redundantXSpace = viewWidth - (scaleX * drawableWidth);
        float redundantYSpace = viewHeight - (scaleY * drawableHeight);
        matchViewWidth = viewWidth - redundantXSpace;
        matchViewHeight = viewHeight - redundantYSpace;
        if (!isZoomed() && !imageRenderedAtLeastOnce) {
            //
            // Stretch and center image to fit view
            //
            matrix.setScale(scaleX, scaleY);
            matrix.postTranslate(redundantXSpace / 2, redundantYSpace / 2);
            normalizedScale = 1;

        } else {
            //
            // These values should never be 0 or we will set viewWidth and viewHeight
            // to NaN in translateMatrixAfterRotate. To avoid this, call savePreviousImageValues
            // to set them equal to the current values.
            //
            if (prevMatchViewWidth == 0 || prevMatchViewHeight == 0) {
                savePreviousImageValues();
            }

            prevMatrix.getElements(m);

            //
            // Rescale Matrix after rotation
            //
            m[0] = matchViewWidth / drawableWidth * normalizedScale;
            m[NUMBER_FOUR] = matchViewHeight / drawableHeight * normalizedScale;

            //
            // TransX and TransY from previous matrix
            //
            float transX = m[2];
            float transY = m[NUMBER_FIVE];

            //
            // Width
            //
            float prevActualWidth = prevMatchViewWidth * normalizedScale;
            float actualWidth = getImageWidth();
            translateMatrixAfterRotate(2, transX, prevActualWidth, actualWidth, prevViewWidth, viewWidth, drawableWidth);

            //
            // Height
            //
            float prevActualHeight = prevMatchViewHeight * normalizedScale;
            float actualHeight = getImageHeight();
            translateMatrixAfterRotate(NUMBER_FIVE, transY, prevActualHeight, actualHeight, prevViewHeight, viewHeight, drawableHeight);

            //
            // Set the matrix to the adjusted scale and translate values.
            //
            matrix.setElements(m);
        }
        fixTrans();
        invalidate();
    }

    /**
     * Set view dimensions based on layout params
     *
     * @param mode
     * @param size
     * @param drawableWidth
     * @return
     */
    private int setViewSize(int mode, int size, int drawableWidth) {
        int viewSize;
        switch (mode) {
            case Component.MeasureSpec.PRECISE:
                viewSize = size;
                break;

            case Component.MeasureSpec.NOT_EXCEED:
                viewSize = Math.min(drawableWidth, size);
                break;

            case Component.MeasureSpec.UNCONSTRAINT:
                viewSize = drawableWidth;
                break;

            default:
                viewSize = size;
                break;
        }
        return viewSize;
    }

    /**
     * After rotating, the matrix needs to be translated. This function finds the area of image
     * which was previously centered and adjusts translations so that is again the center, post-rotation.
     *
     * @param axis Matrix.MTRANS_X or Matrix.MTRANS_Y
     * @param trans the value of trans in that axis before the rotation
     * @param prevImageSize the width/height of the image before the rotation
     * @param imageSize width/height of the image after rotation
     * @param prevViewSize width/height of view before rotation
     * @param viewSize width/height of view after rotation
     * @param drawableSize width/height of drawable
     */
    private void translateMatrixAfterRotate(int axis, float trans, float prevImageSize, float imageSize, int prevViewSize, int viewSize, int drawableSize) {
        if (imageSize < viewSize) {
            //
            // The width/height of image is less than the view's width/height. Center it.
            //
            m[axis] = (viewSize - (drawableSize * m[0])) * HELF_RATIO;

        } else if (trans > 0) {
            //
            // The image is larger than the view, but was not before rotation. Center it.
            //
            m[axis] = -((imageSize - viewSize) * HELF_RATIO);

        } else {
            //
            // Find the area of the image which was previously centered in the view. Determine its distance
            // from the left/top side of the view as a fraction of the entire image's width/height. Use that percentage
            // to calculate the trans in the new view width/height.
            //
            float percentage = (Math.abs(trans) + (HELF_RATIO * prevViewSize)) / prevImageSize;
            m[axis] = -((percentage * imageSize) - (viewSize * HELF_RATIO));
        }
    }

    private void setState(State stateTemp) {
        this.state = stateTemp;
    }

    public final boolean canScrollHorizontallyFroyo(int direction) {
        return canScrollHorizontally(direction);
    }

    public final boolean canScrollHorizontally(int direction) {
        matrix.getElements(m);
        float x = m[2];

        if (getImageWidth() < viewWidth) {
            return false;

        } else if (x >= -1 && direction < 0) {
            return false;

        } else if (Math.abs(x) + viewWidth + 1 >= getImageWidth() && direction > 0) {
            return false;
        }

        return true;
    }

    /**
     * Gesture Listener detects a single click or long click and passes that on
     * to the view's listener.
     * @author Ortiz
     *
     */

    public boolean onFling(TouchEvent e1, TouchEvent e2, float velocityX, float velocityY)
    {
        if (fling != null) {
            //
            // If a previous fling is still active, it should be cancelled so that two flings
            // are not run simultaenously.
            //
            fling.cancelFling();
        }
        fling = new Fling((int) velocityX, (int) velocityY);
        compatPostOnAnimation(fling);
        return true;
    }

    public final boolean onDoubleTap(TouchEvent e) {
        boolean consumed = false;
        if (state == State.NONE) {
            float targetZoom = (normalizedScale == minScale) ? maxScale : minScale;
            DoubleTapZoom doubleTap = new DoubleTapZoom(targetZoom, e.getPointerPosition(e.getIndex()).getX(), e.getPointerPosition(e.getIndex()).getY(), false);
            compatPostOnAnimation(doubleTap);
            consumed = true;
            setState(State.ZOOM);
            previousUpEventTime = 0;
        }
        return consumed;
    }

    private void scaleImage(double deltaScale, float focusX, float focusY, boolean stretchImageToSuper) {

        float lowerScale, upperScale;
        if (stretchImageToSuper) {
            lowerScale = superMinScale;
            upperScale = superMaxScale;

        } else {
            lowerScale = minScale;
            upperScale = maxScale;
        }

        float origScale = normalizedScale;
        double deltaScaleTemp = deltaScale;
        normalizedScale *= deltaScale;
        if (normalizedScale > upperScale) {
            normalizedScale = upperScale;
            deltaScaleTemp = upperScale / origScale;
        } else if (normalizedScale < lowerScale) {
            normalizedScale = lowerScale;
            deltaScaleTemp = lowerScale / origScale;
        }

        matrix.postScale((float) deltaScaleTemp, (float) deltaScaleTemp, focusX, focusY);
        fixScaleTrans();
    }

    /**
     * This function will transform the coordinates in the touch event to the coordinate
     * system of the drawable that the imageview contain
     * @param x x-coordinate of touch event
     * @param y y-coordinate of touch event
     * @param clipToBitmap Touch event may occur within view, but outside image content. True, to clip return value
     * 			to the bounds of the bitmap size.
     * @return Coordinates of the point touched, in the coordinate system of the original drawable.
     */
    private Quaternion transformCoordTouchToBitmap(float x, float y, boolean clipToBitmap) {
        matrix.getElements(m);
        float origW = getPixelMap().getImageInfo().size.width;  //imageview
        float origH = getPixelMap().getImageInfo().size.height; //imageview
        float transX = m[NUMBER_TWO];
        float transY = m[NUMBER_FIVE];
        float finalX = ((x - transX) * origW) / getImageWidth();
        float finalY = ((y - transY) * origH) / getImageHeight();

        if (clipToBitmap) {
            finalX = Math.min(Math.max(finalX, 0), origW);
            finalY = Math.min(Math.max(finalY, 0), origH);
        }

        return new Quaternion(finalX , finalY,0,0);
    }

    /**
     * Inverse of transformCoordTouchToBitmap. This function will transform the coordinates in the
     * drawable's coordinate system to the view's coordinate system.
     * @param bx x-coordinate in original bitmap coordinate system
     * @param by y-coordinate in original bitmap coordinate system
     * @return Coordinates of the point in the view's coordinate system.
     */
    private Quaternion transformCoordBitmapToTouch(float bx, float by) {
        matrix.getElements(m);
        float origW = getPixelMap().getImageInfo().size.width;
        float origH = getPixelMap().getImageInfo().size.height;
        float px = bx / origW;
        float py = by / origH;
        float finalX = m[NUMBER_TWO] + getImageWidth() * px;
        float finalY = m[NUMBER_FIVE] + getImageHeight() * py;
        return new Quaternion(finalX , finalY,0,0);
    }

    private void compatPostOnAnimation(Runnable runnable) {
        uIthread.asyncDispatch(runnable);
    }

    private void printMatrixInfo() {
        float[] n = new float[NUMBER_NINE];
        matrix.getElements(n);
    }

    private boolean isConsideredDoubleTap(TouchEvent secondDown) {
        final long deltaTime = secondDown.getOccurredTime() - previousUpEventTime;
        return deltaTime <= MAX_DELTA_TIME && deltaTime >= MIN_DELTA_TIME;
    }

    private class privateEstimateSizeListener implements EstimateSizeListener{

        @Override
        public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
            PixelMap drawable = getPixelMap();
            if (drawable == null || drawable.getImageInfo().size.width == 0 || drawable.getImageInfo().size.height == 0) {
                setEstimatedSize(0, 0);
                return true;
            }

            int drawableWidth = drawable.getImageInfo().size.width;
            int drawableHeight = drawable.getImageInfo().size.height;
            int widthSize = Component.MeasureSpec.getSize(widthMeasureSpec);
            int widthMode = Component.MeasureSpec.getMode(widthMeasureSpec);
            int heightSize = Component.MeasureSpec.getSize(heightMeasureSpec);
            int heightMode = Component.MeasureSpec.getMode(heightMeasureSpec);
            viewWidth = setViewSize(widthMode, widthSize, drawableWidth);
            viewHeight = setViewSize(heightMode, heightSize, drawableHeight);

            //
            // Set view dimensions
            //
            setEstimatedSize(viewWidth, viewHeight);

            //
            // Fit content within view
            //
            fitImageToView();
            return false;
        }
    }

    private class PrivateOnScaleListener implements ScaledListener{
        @Override
        public void onScaleStart(Component component, ScaleInfo scaleInfo) {
            setState(State.ZOOM);
        }

        @Override
        public void onScaleUpdate(Component component, ScaleInfo scaleInfo) {
            scaleImage(Math.pow(scaleInfo.scale,1./FIX_SCALE), scaleInfo.updatePoint.getPointX(), scaleInfo.updatePoint.getPointY(), true);

            if (touchImageViewListener != null) {
                touchImageViewListener.onMove();
            }
        }

        @Override
        public void onScaleEnd(Component component, ScaleInfo scaleInfo) {
            setState(State.NONE);
            boolean animateToZoomBoundary = false;
            float targetZoom = normalizedScale;
            if (normalizedScale > maxScale) {
                targetZoom = maxScale;
                animateToZoomBoundary = true;

            } else if (normalizedScale < minScale) {
                targetZoom = minScale;
                animateToZoomBoundary = true;
            }

            if (animateToZoomBoundary) {
                DoubleTapZoom doubleTap = new DoubleTapZoom(targetZoom, viewWidth / 2, viewHeight / 2, true);
                compatPostOnAnimation(doubleTap);
            }
        }
    }

    /**
     * Responsible for all touch events. Handles the heavy lifting of drag and also sends
     * touch events to Scale Detector and Gesture Detector.
     * @author Ortiz
     *
     */
    private class PrivateOnTouchListener implements TouchEventListener {

        //
        // Remember last point position for dragging
        //
        private Point last = new Point();

        @Override
        public boolean onTouchEvent(Component v, TouchEvent event) {
            if (mVelocityDetector == null) {
                mVelocityDetector = VelocityDetector.obtainInstance();
            }
            mVelocityDetector.addEvent(event);
            mVelocityDetector.calculateCurrentVelocity(UNITS, MAX_VX_VELOCITY, MAX_VY_VELOCITY);
            float velocityX = mVelocityDetector.getHorizontalVelocity();
            float velocityY = mVelocityDetector.getVerticalVelocity();
            Point curr = new Point(event.getPointerPosition(event.getIndex()).getX(),event.getPointerPosition(event.getIndex()).getY());

            if (state == State.NONE || state == State.DRAG || state == State.FLING) {
                switch (event.getAction()) {
                    case TouchEvent.PRIMARY_POINT_DOWN:
                        last.modify(curr);
                        mCurrentDownEvent = event;
                        if (fling != null)
                            fling.cancelFling();
                        if(previousUpEventTime != 0 && isConsideredDoubleTap(event)){
                            onDoubleTap(event);
                        }else{
                            setState(State.DRAG);
                        }
                        break;

                    case TouchEvent.POINT_MOVE:
                        float fy = curr.getPointY() - last.getPointY();
                        float fx = curr.getPointX() - last.getPointX();
                        if(state == State.DRAG){
                            if ((Math.abs(fx) > MIN_DISTANCE ||Math.abs(fy) > MIN_DISTANCE)&&((Math.abs(velocityY) < VY_VELOCITY)
                                    && (Math.abs(velocityX) < VX_VELOCITY))) {
                                float deltaX = curr.getPointX() - last.getPointX();
                                float deltaY = curr.getPointY() - last.getPointY();
                                float fixTransX = getFixDragTrans(deltaX, viewWidth, getImageWidth());
                                float fixTransY = getFixDragTrans(deltaY, viewHeight, getImageHeight());
                                matrix.postTranslate(fixTransX, fixTransY);
                                fixTrans();
                                last.modify(curr.getPointX(),curr.getPointY());
                            }
                        }
                        break;

                    case TouchEvent.PRIMARY_POINT_UP:
                        if (((Math.abs(velocityY) >= VY_VELOCITY)
                                || (Math.abs(velocityX) >= VX_VELOCITY))&&((abs(curr.getPointY()-last.getPointY())> MIN_THRESHOLD)||(abs(curr.getPointX()-last.getPointX())> MIN_THRESHOLD))){
                            if(getImageWidth() < viewWidth){
                                velocityX = 0;
                            }
                            onFling(mCurrentDownEvent, event, 2f*velocityX, 2f*velocityY);
                        }
                        previousUpEventTime = event.getOccurredTime();
                        setState(State.NONE);
                        break;
                    case TouchEvent.TOUCH_PANEL:
                }
            }
            invalidate();

            //
            // User-defined OnTouchListener
            //
            if(userTouchListener != null) {
                userTouchListener.onTouchEvent(v, event);
            }

            //
            // OnTouchImageViewListener is set: TouchImageView dragged by user.
            //
            if (touchImageViewListener != null) {
                touchImageViewListener.onMove();
            }

            //
            // indicate event was handled
            //
            return true;
        }
    }

    /**
     * DoubleTapZoom calls a series of runnables which apply
     * an animated zoom in/out graphic to the image.
     * @author Ortiz
     *
     */
    private class DoubleTapZoom implements Runnable {

        private long startTime;
        private static final float ZOOM_TIME = 500;
        private float startZoom, targetZoom;
        private float bitmapX, bitmapY;
        private boolean stretchImageToSuper;
        //private Animator interpolator;
        private Quaternion startTouch;
        private Quaternion endTouch;

        DoubleTapZoom(float targetZoomTemp, float focusX, float focusY, boolean stretchImageToSuperTemp) {
            setState(State.ANIMATE_ZOOM);
            startTime = System.currentTimeMillis();
            this.startZoom = normalizedScale;
            this.targetZoom = targetZoomTemp;
            this.stretchImageToSuper = stretchImageToSuperTemp;
            Quaternion bitmapPoint = transformCoordTouchToBitmap(focusX, focusY, false);
            this.bitmapX = bitmapPoint.getX();
            this.bitmapY = bitmapPoint.getY();

            //
            // Used for translating image during scaling
            //
            startTouch = transformCoordBitmapToTouch(bitmapX, bitmapY);
            endTouch = new Quaternion(viewWidth / 2, viewHeight / 2,0,0);
        }

        @Override
        public void run() {
            float t = interpolate();
            double deltaScale = calculateDeltaScale(t);
            scaleImage(deltaScale, bitmapX, bitmapY, stretchImageToSuper);
            translateImageToCenterTouchPosition(t);
            fixScaleTrans();
            invalidate();

            //
            // OnTouchImageViewListener is set: double tap runnable updates listener
            // with every frame.
            //
            if (touchImageViewListener != null) {
                touchImageViewListener.onMove();
            }

            if (t < 1f) {
                //
                // We haven't finishjkhjkhed zooming
                //
                compatPostOnAnimation(this);
                //Animator

            } else {
                //
                // Finished zooming
                //
                setState(State.NONE);
            }
        }

        /**
         * Interpolate between where the image should start and end in order to translate
         * the image so that the point that is touched is what ends up centered at the end
         * of the zoom.
         * @param t
         */
        private void translateImageToCenterTouchPosition(float t) {
            float targetX = startTouch.getX() + t * (endTouch.getX() - startTouch.getX());
            float targetY = startTouch.getY() + t * (endTouch.getY() - startTouch.getY());
            Quaternion curr = transformCoordBitmapToTouch(bitmapX, bitmapY);
            matrix.postTranslate(targetX - curr.getX(), targetY - curr.getY());
        }

        /**
         * Use interpolator to get t
         * @return
         */
        private float interpolate() {
            long currTime = System.currentTimeMillis();     //未找到相关类  动画相关
            float elapsed = (currTime - startTime) / ZOOM_TIME;
            elapsed = Math.min(1f, elapsed);
            return getInterpolation(elapsed);
        }

        private float getInterpolation(float input) {
            return (float) (Math.cos((input + 1) * Math.PI) / 2.0f) + HELF_RATIO;
        }

        /**
         * Interpolate the current targeted zoom and get the delta
         * from the current zoom.
         * @param t
         * @return
         */
        private double calculateDeltaScale(float t) {
            double zoom = startZoom + t * (targetZoom - startZoom);
            return zoom / normalizedScale;
        }
    }

    private class CompatScroller {
        private ScrollHelper overScroller;
        private boolean isPreGingerbread;

        CompatScroller(Context contextTemp) {
            isPreGingerbread = false;
            overScroller = new ScrollHelper();
        }

        public void fling(int startX, int startY, int velocityX, int velocityY, int minX, int maxX, int minY, int maxY) {
            overScroller.doFling(startX, startY, velocityX, velocityY, minX, maxX, minY, maxY);
        }

        public void forceFinished(boolean finished) {
            overScroller.abortAnimation();
        }

        public boolean isFinished() {
            return overScroller.isFinished();
        }

        public boolean computeScrollOffset() {
            return overScroller.updateScroll();

        }

        public int getCurrX() {
            return overScroller.getCurrValue(ScrollHelper.AXIS_X);
        }

        public int getCurrY() {
            return overScroller.getCurrValue(ScrollHelper.AXIS_Y);
        }
    }

    /**
     * Fling launches sequential runnables which apply
     * the fling graphic to the image. The values for the translation
     * are interpolated by the Scroller.
     * @author Ortiz
     *
     */
    private class Fling implements Runnable {

        private CompatScroller scroller;
        private int currX, currY;

        Fling(int velocityX, int velocityY) {
            setState(State.FLING);
            scroller = new CompatScroller(context);
            matrix.getElements(m);

            int startX = (int) m[2];
            int startY = (int) m[NUMBER_FIVE];
            int minX, maxX, minY, maxY;

            if (getImageWidth() > viewWidth) {
                minX = viewWidth - (int) getImageWidth();
                maxX = 0;
            } else {
                minX = startX;
                maxX = startX;
            }

            if (getImageHeight() > viewHeight) {
                minY = viewHeight - (int) getImageHeight();
                maxY = 0;
            } else {
                minY = startY;
                maxY = startY;
            }
            currX = startX;
            currY = startY;
            scroller.fling(startX, startY, (int) velocityX, (int) velocityY, minX,
                    maxX, minY+FIX_LENGTH, maxY);

        }

        public void cancelFling() {
            if (scroller != null) {
                setState(State.NONE);
                scroller.forceFinished(true);
            }
        }

        @Override
        public void run() {
            //
            // OnTouchImageViewListener is set: TouchImageView listener has been flung by user.
            // Listener runnable updated with each frame of fling animation.
            //
            if (touchImageViewListener != null) {
                touchImageViewListener.onMove();
            }

            if (scroller.isFinished()) {
                scroller = null;
                return;
            }

            if (scroller.computeScrollOffset()) {
                int newX = scroller.getCurrX();
                int newY = scroller.getCurrY();
                int transX = newX - currX;
                int transY = newY - currY;
                currX = newX;
                currY = newY;
                matrix.postTranslate(transX, transY);
                fixTran();
                if(stop){
                    scroller.forceFinished(true);
                }
                invalidate();
                compatPostOnAnimation(this);
                stop = false;
            }
        }
    }

    private class ZoomVariables {
        private float scale;
        private float focusX;
        private float focusY;
        private ScaleMode scaleType;

        ZoomVariables(float scaleTemp, float focusXTemp, float focusYTemp, ScaleMode scaleTypeTemp) {
            this.scale = scaleTemp;
            this.focusX = focusXTemp;
            this.focusY = focusYTemp;
            this.scaleType = scaleTypeTemp;
        }
    }

    public interface OnTouchImageViewListener {
        void onMove();
    }
}

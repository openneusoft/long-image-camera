package com.wajahatkarim3.longimagecamera.base;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.agp.graphics.Surface;
import ohos.agp.graphics.SurfaceOps;
import ohos.agp.graphics.TextureHolder;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Matrix;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.common.ImageFormat;

public class TextureViewPreview extends PreviewImpl {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.DEBUG, 0x00201, TextureViewPreview.class.getName());   //LABLE_lOG
    private SurfaceProvider mSurfaceProvider;
    private Surface mSurface;

    private int mDisplayOrientation;

    private static final int DEGREE_RIGHT_ANGLE = 90;
    private static final int DEGREE_FLAT_ANGLE = 180;

    private static final int POINT_COUNT = 180;


    public TextureViewPreview(Context context) {

        mSurfaceProvider = new SurfaceProvider(context);

        StackLayout.LayoutConfig layoutConfig = new StackLayout.LayoutConfig();
        layoutConfig.alignment = LayoutAlignment.CENTER;
        layoutConfig.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
        layoutConfig.height = ComponentContainer.LayoutConfig.MATCH_PARENT;

        mSurfaceProvider.setLayoutConfig(layoutConfig);

        mSurfaceProvider.pinToZTop(false);

        mSurfaceProvider.getSurfaceOps().get().setFormat(ImageFormat.JPEG);
        // 设置屏幕一直打开
        mSurfaceProvider.getSurfaceOps().get().setKeepScreenOn(true);

        mSurface =  mSurfaceProvider.getSurfaceOps().get().getSurface();
    }

    @Override
    public final void setBufferSize(int width, int height) {
        mSurfaceProvider.getSurfaceOps().get().setFixedSize(width, height);
    }

    @Override
    public final SurfaceProvider getSurfaceProvider() {
        return mSurfaceProvider;
    }

    @Override
    public final SurfaceOps getSurfaceOps() {

        return mSurfaceProvider.getSurfaceOps().get();
    }

    @Override
    public final TextureHolder getSurfaceTexture() {
        return null;
    }

    @Override
    public final Component getView() {
        return mSurfaceProvider;
    }

    @Override
    public final Class getOutputClass() {
        return TextureViewPreview.class;
    }

    @Override
    public final boolean isReady() {
        return mSurfaceProvider.getSurfaceOps().get().getSurface() != null;
    }

    @Override
    public final void setDisplayOrientation(Size size) {
        StackLayout.LayoutConfig layoutConfig = new StackLayout.LayoutConfig();
        layoutConfig.alignment = LayoutAlignment.CENTER;
        layoutConfig.width = size.getWidth();
        layoutConfig.height = size.getHeight();
        mSurfaceProvider.setLayoutConfig(layoutConfig);
    }

    final void configureTransform() {
        Matrix matrix = new Matrix();
        if (mDisplayOrientation % DEGREE_FLAT_ANGLE == DEGREE_RIGHT_ANGLE) {
            final int width = getWidth();
            final int height = getHeight();
            // Rotate the camera preview when the screen is landscape.
            matrix.setPolyToPoly(
                    new float[]{
                            0.f, 0.f, // top left
                            width, 0.f, // top right
                            0.f, height, // bottom left
                            width, height, // bottom right
                    }, 0,
                    mDisplayOrientation == DEGREE_RIGHT_ANGLE ?
                            // Clockwise
                            new float[]{
                                    0.f, height, // top left
                                    0.f, 0.f, // top right
                                    width, height, // bottom left
                                    width, 0.f, // bottom right
                            } : // mDisplayOrientation == 270
                            // Counter-clockwise
                            new float[]{
                                    width, 0.f, // top left
                                    width, height, // top right
                                    0.f, 0.f, // bottom left
                                    0.f, height, // bottom right
                            }, 0,
                    POINT_COUNT);
        } else if (mDisplayOrientation == DEGREE_FLAT_ANGLE) {
            matrix.postRotate(DEGREE_FLAT_ANGLE, getWidth() / 2, getHeight() / 2);
        }
//        mTextureView.setTransform(matrix);
    }

    private class TextureSurfaceCallback implements SurfaceOps.Callback {

        @Override
        public void surfaceCreated(SurfaceOps surfaceOps) {
            mSurface = surfaceOps.getSurface();
            if (mSurface == null){
                HiLog.info(LABEL,"mSurface null");
            }else {
                HiLog.info(LABEL,"mSurface have");
            }

//            CameraKit cameraKit = CameraKit.getInstance(abilitySlice.getContext());

//            configureTransform();
        }

//        public void surfaceCreated(SurfaceOps surfaceOps, int width, int height) {
//            setSize(width, height);
//            configureTransform();
//            dispatchSurfaceChanged();
//        }

        @Override
        public void surfaceChanged(SurfaceOps surfaceOps, int i, int i1, int i2) {
//            setSize(i1, i2);
//            configureTransform();
//            dispatchSurfaceChanged();
        }

        @Override
        public void surfaceDestroyed(SurfaceOps surfaceOps) {
            setSize(0, 0);
        }
    }
}

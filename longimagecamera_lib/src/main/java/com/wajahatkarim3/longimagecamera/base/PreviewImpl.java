package com.wajahatkarim3.longimagecamera.base;

import ohos.agp.components.Component;
import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.agp.graphics.SurfaceOps;
import ohos.agp.graphics.TextureHolder;

/**
 * Encapsulates all the operations related to camera preview in a backward-compatible manner.
 */
public abstract class PreviewImpl {


    private Callback mCallback;

    private int mWidth;

    private int mHeight;

    final void setCallback(Callback callback) {
        mCallback = callback;
    }

    public abstract SurfaceProvider getSurfaceProvider();

    public abstract SurfaceOps getSurfaceOps();

    public abstract Component getView();

    public abstract Class getOutputClass();

    public abstract boolean isReady();

    public abstract void setDisplayOrientation(Size size);

    public final void dispatchSurfaceChanged() {
        mCallback.onSurfaceChanged();
    }

    final TextureHolder getSurfaceHolder() {
        return null;
    }

    public abstract Object getSurfaceTexture();

    public void setBufferSize(int width, int height) {
    }

    public final void setSize(int width, int height) {
        mWidth = width;
        mHeight = height;
    }

    public final int getWidth() {
        return mWidth;
    }

    public final int getHeight() {
        return mHeight;
    }

    interface Callback {
        void onSurfaceChanged();
    }

}


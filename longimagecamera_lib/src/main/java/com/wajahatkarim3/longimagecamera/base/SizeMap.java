package com.wajahatkarim3.longimagecamera.base;

import java.util.HashMap;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class SizeMap {
    private final HashMap<AspectRatio, SortedSet<Size>> mRatios = new HashMap<>();

    /**
     * Add a new {@link Size} to this collection.
     *
     * @param size The size to add.
     * @return {@code true} if it is added, {@code false} if it already exists and is not added.
     */
    public boolean add(Size size) {
        for (AspectRatio ratio : mRatios.keySet()) {
            if (ratio.matches(size)) {
                final SortedSet<Size> sizes = mRatios.get(ratio);
                if (sizes.contains(size)) {
                    return false;
                } else {
                    sizes.add(size);
                    return true;
                }
            }
        }
        // None of the existing ratio matches the provided size; add a new key
        SortedSet<Size> sizes = new TreeSet<>();
        sizes.add(size);
        mRatios.put(AspectRatio.of(size.getWidth(), size.getHeight()), sizes);
        return true;
    }

    /**
     * Removes the specified aspect ratio and all sizes associated with it.
     *
     * @param ratio The aspect ratio to be removed.
     */
    public void remove(AspectRatio ratio) {
        mRatios.remove(ratio);
    }

    public final Set<AspectRatio> ratios() {
        return mRatios.keySet();
    }

    public final SortedSet<Size> sizes(AspectRatio ratio) {
        return mRatios.get(ratio);
    }

    public final void clear() {
        mRatios.clear();
    }

    final boolean isEmpty() {
        return mRatios.isEmpty();
    }
}

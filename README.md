# LongImageCamera

A camera view to capture long image merged from small captured images  .This project is based on the open source project LongImageCamera for ohos migration and development, through the project tag and GitHub address（ https://github.com/wajahatkarim3/LongImageCamera）Track to original project version. This library includes:

    Built-in Camera UI and activity
    Built-in Image Viewer with pinch-fling gestures for zoom/move/rotate etc.
    TouchImageView with gestures support for zoom/move/rotate etc.
    Built-in permission handling for Camera and Storage
    Horizontal or Vertical image merging


Original project doc address:https://github.com/wajahatkarim3/LongImageCamera

Project author and maintainer: Wajahat Karim

- Website (http://wajahatkarim.com)
- Twitter (http://twitter.com/wajahatkarim)
- Medium (http://www.medium.com/@wajahatkarim3)
- LinkedIn (http://www.linkedin.com/in/wajahatkarim)

Original project baseline version: v1.0.2 sha1:fd257878745ecd82fb0e2d3871bb07f5d952399b

Programming language: Java

## DEMO

![](demo.gif)

Install Demo app or HAR from Releases on your device and try to capture a long image!

## Version

v1.0.1

## Installation

 Method 1:

1. Build longimagecamera_lib.har

2. Add LongImageCamera as a new dependency inside your entry's build.gradle:

```json
dependencies {
    implementation project(":longimagecamera_lib")
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    testImplementation 'junit:junit:4.13'
    ohosTestImplementation 'com.huawei.ohos.testkit:runner:1.0.0.100'
}
```

Method 2:

1. Include maven of "s01.oss.sonatype.org" in project's build.gradle:

```json
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/releases/' 
    }
}
   
```

2. Add LongImageCamera as a new dependency inside your entry's build.gradle:

```json
dependencies {
    implementation 'io.github.dzsf:longimagecamera:1.0.1'
}
```

## Usage

Launch Camera Ability

Start the camera from any Ability with this code:

```java
Operation operation = new Intent.OperationBuilder()
    .withBundleName("com.ohos.longimagecamera")
    .withAbilityName("com.ohos.longimagecamera_lib.LongImageAbility")
    .build();
    secondIntent.setOperation(operation);
    startAbilityForResult(secondIntent, 0);
and then you will get the result image in onAbilityResult() method like this:
protected void onAbilityResult(int requestCode, int resultCode, Intent resultData){...}
```

## Launch Image Viewer Ability

This Ability comes with a built-in image viewer including support of pinch/fling gestures for zoom/move etc. You can launch image viewer with this code:

```java
Intent ii = new Intent();
ii.setAction("action.previewlongimage");
startAbility(ii);
```

Using TouchImageView in your layouts

You can use customized ImageView with support of gestures for zoom/move/rotate etc like this:

```xml
<com.wajahatkarim3.longimagecamera.cameraview.TouchImageView
    ohos:id="$+id:imageView"
    ohos:width="match_parent"
    ohos:height="match_parent"
    ohos:below ="$id:button_start"
    ohos:margin="4vp"
        />

```

## License

Copyright 2017 Wajahat Karim

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.



